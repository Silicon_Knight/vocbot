<?php
include('date.php'); //Gets the date/time the docker container was built

/*
So what the fuck is this?  This is a simple script to integrate into SLACK and post as a bot any new VOC data that comes from the feed.  
Psudo code:
Load the JSON -> Make a hash for each line -> Check to see if the hash exists in a simple CSV file (to make sure the bot doesnt post dupes) -> Post to slack -> Drink$

Why am I using CSV? Its easy.  We can move to a MySQL dbase if needed but for now, its a quick and dirty MVP.

Feb 2016 - Coded by Aron Reid (aron.reid@telus.com)     
*/

//Lets grab all the needed env vars set by the compose file
$slackToken = getenv('SLACKTOKEN');
$qualUser = getenv('QUALUSER');
$qualToken = getenv('QUALTOKEN');
$qualSurvID = getenv('SURVID');
$channel = getenv('CHANNEL');
$botname = getenv('BOTNAME');

//Lets set the timezone to UTC (same as AWS EC2 instances)
date_default_timezone_set('UTC');

$debug = 1; //0 off 1 on mutes SLACK
$fileName = '/home/slackbot.csv'; //Name of the CSV file used for the hash so I dont repeat the same things over and over

$token = 0;  //tracking token just incase I need it some time here....
$hash = array (
        'id' => null,
        'hash' => null,
); // Hash array is used to create the CSV file

$qualLimit = 25;

//Lets figure out when the container started so we dont pull any VOC comments before that time.  I.e. Only new comments.
echo "\n\n".urldecode($startDate)."\n\n";
$start_mo_array = explode("|", $startDate);
$start_mo_arry2 = explode("/", $start_mo_array[1]);
$start_day = $start_mo_arry2[0];
$start_month = $start_mo_arry2[1];
$start_year = $start_mo_arry2[2];

$start_time = explode(":", $start_mo_array[0]);
$start_hour = $start_time[0];
$start_min = $start_time[1];
$start_sec = $start_time[2];
// Okay now we know the minutes/seconds the container started

$dateDay = date('d') -2; //Lets parse out anything that is older than 2 days ago
$dateDay = str_pad($dateDay, 2, "0", STR_PAD_LEFT); //Padding cause PHP
echo $dateDay . " / / ";
if ($dateDay < 0) {$dateDay =0;};
$qualStart = date('Y')."-".date('m')."-".$dateDay."%2000%3A00%3A00";

echo $qualStart;

//END
$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    ),
);  
